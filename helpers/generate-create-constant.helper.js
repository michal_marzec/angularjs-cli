const fs = require('fs');
const util = require('util');
const path = require('path');

module.exports = async (options) => {
    const exists = util.promisify(fs.exists);
    const mkdir = util.promisify(fs.mkdir);
    const writeFile = util.promisify(fs.writeFile);
    const readFile = util.promisify(fs.readFile);
    const appendFile = util.promisify(fs.appendFile);

    options.directory = path.resolve(options.directory + '/..');

    const constExists = await exists(path.resolve(`${options.directory}/${options.names.kebap}.constant.js`));
    if (constExists) throw new Error('Constant already exists');

    const dirExists = await exists(options.directory);

    if (!dirExists) await mkdir(options.directory);

    let contentJs = await readFile(`${options.contentPath}constant-js.content`, 'utf-8');

    contentJs = contentJs.replace(/:nameCamel/g, options.names.camel);
    contentJs = contentJs.replace(/:nameCapitalized/g, options.names.capitalized);

    if (options.root) {
        const content = await readFile(`${options.directory}/../../../app.imports.js`, 'utf-8');
        let splitted = content.split('\n'),
            line = null,
            length = splitted.length;
        for(let i = 0; i < length; ++i) {
            if (splitted[i].indexOf('module') > -1) line = i + 1;
        }

        if (line !== null) {
            splitted.splice(line, 0, `\nrequire('./src/app/constants/${options.names.kebap}.constant.js');`);

            const newContent = splitted.join('\n');

            await writeFile(`${options.directory}/../../../app.imports.js`, newContent);
        } else {
            await appendFile(`${options.directory}/../../../app.imports.js`, `\nrequire('./src/app/constants/${options.names.kebap}.constant.js');`)
        }

    }

    await writeFile(`${options.directory}/${options.names.kebap}.constant.js`, contentJs);
};
