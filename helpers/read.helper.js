const READ = require('./../interfaces/read.interface.js');

module.exports = function (arguments, spinner) {
    spinner.text = 'Reading command... ';
    let result;

    switch (arguments[0]) {
        case READ.MAIN.NEW:
            result = readNew(arguments);
            break;
        case READ.MAIN.GENERATE:
            result = readGenerate(arguments);
            break;
        case READ.MAIN.GENERATE_SHORT:
            result = readGenerate(arguments);
            break;
        case READ.MAIN.INFO:
            result = readInfo();
            break;
        default:
            throw new Error('Cannot determine type of command');
    }

    return result;
};

function readNew(arguments) {
    if (!arguments[1]) throw new Error('Name not provided');
    return {
        command: READ.COMMANDS.NEW,
        names: {
            camel: toCamel(arguments[1]),
            snake: toSnake(arguments[1]),
            kebap: toKebap(arguments[1])
        }
    }
}

function readGenerate(arguments) {
    if (!arguments[1]) throw new Error('Type not provided');
    let validType = false;
    for(let i in READ.TYPES) {
        if (READ.TYPES.hasOwnProperty(i)) {
            if (READ.TYPES[i] === arguments[1]) {
                validType = true;
            }
        }
    }
    if (!validType) throw new Error('Invalid type, available types: component, directive, filter, constant');
    if (!arguments[2]) throw new Error('Name not provided');
    return {
        command: READ.COMMANDS.GENERATE,
        type: arguments[1],
        names: {
            camel: toCamel(arguments[2]),
            snake: toSnake(arguments[2]),
            kebap: toKebap(arguments[2]),
            capitalized: toCapitalized(arguments[2])
        }
    }
}

function readInfo() {
    return {
        command: READ.COMMANDS.INFO
    };
}

function splitOriginalName(name) {
    return name.split(/(?=[A-Z])|[^\w\s]/g);
}

function toCamel(name) {
    const splitted = splitOriginalName(name);
    return splitted.map(value => {
        return value.charAt(0).toUpperCase() + value.substr(1);
    }).join('');
}

function toSnake(name) {
    const splitted = splitOriginalName(name);
    return splitted.map(value => {
        return value.toLowerCase();
    }).join('_');
}

function toKebap(name) {
    const splitted = splitOriginalName(name);
    return splitted.map(value => {
       return value.toLowerCase();
    }).join('-');
}

function toCapitalized(name) {
    const splitted = splitOriginalName(name);
    return splitted.map(value => {
        return value.toUpperCase();
    }).join('_');
}
