const fs = require('fs');
const util = require('util');
const execute = require('child_process').exec;

module.exports = async (options) => {
    let exec;
    try {
        exec = util.promisify(execute);

        await exec(`cd ${options.names.kebap} && yarn install;`)
    } catch (e) {
        throw e;
    }
};