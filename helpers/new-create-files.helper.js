const fs = require('fs');
const util = require('util');

function getToCreate(options) {
    return [
        /**
         * .angularjs-cli.json
         */
        {
            contentPath: __dirname + '/../contents/config.content',
            replace: [
                {
                    regex: /:nameCamel/g,
                    value: options.names.camel
                },
                {
                    regex: /:nameKebap/g,
                    value: options.names.kebap
                },
                {
                    regex: /:nameSnake/g,
                    value: options.names.snake
                }
            ],
            writePath: '/.angularjs-cli.json'
        },
        /**
         * .gitignore
         */
        {
            contentPath: __dirname + '/../contents/gitignore.content',
            writePath: '/.gitignore'
        },
        /**
         * package.json
         */
        {
            contentPath: __dirname + '/../contents/package.content',
            replace: [
                {
                    regex: /:nameKebap/g,
                    value: options.names.kebap
                }
            ],
            writePath: '/package.json'
        },
        /**
         * webpack.config.js
         */
        {
            contentPath: __dirname + '/../contents/webpack-config.content',
            writePath: '/webpack.config.js'
        },
        /**
         * app.imports.js
         */
        {
            contentPath: __dirname + '/../contents/app-imports.content',
            writePath: '/app.imports.js'
        },
        /**
         * vendor.imports.js
         */
        {
            contentPath: __dirname + '/../contents/vendor-imports.content',
            writePath: '/vendor.imports.js'
        },
        /**
         * webpack.server.config.js
         */
        {
            contentPath: __dirname + '/../contents/webpack-server-config.content',
            writePath: '/webpack.server.config.js'
        },
        /**
         * index.html
         */
        {
            contentPath: __dirname + '/../contents/index.content',
            replace: [
                {
                    regex: /:nameCamel/g,
                    value: options.names.camel
                }
            ],
            writePath: '/src/index.html'
        },
        /**
         * styles.scss
         */
        {
            contentPath: __dirname + '/../contents/styles.content',
            writePath: '/src/styles.scss'
        },
        /**
         * .gitkeep
         */
        {
            writePath: '/src/assets/.gitkeep'
        },
        /**
         * app.module.js
         */
        {
            contentPath: __dirname + '/../contents/module.content',
            replace: [
                {
                    regex: /:nameCamel/g,
                    value: options.names.camel
                }
            ],
            writePath: '/src/app/app.module.js'
        },
        /**
         * app.routes.js
         */
        {
            contentPath: __dirname + '/../contents/routes.content',
            writePath: '/src/app/app.routes.js'
        },
        /**
         * components.scss
         */
        {
            contentPath: __dirname + '/../contents/styles-components.content',
            writePath: '/src/app/styles/components.scss'
        },
        /**
         * global.scss
         */
        {
            writePath: '/src/app/styles/global.scss'
        },
        /**
         * root.component.js
         */
        {
            contentPath: __dirname + '/../contents/component-js.content',
            replace: [
                {
                    regex: /:nameCamel/g,
                    value: 'Root'
                },
                {
                    regex: /:nameKebap/g,
                    value: 'root'
                }
            ],
            writePath: '/src/app/components/root/root.component.js'
        },
        /**
         * root.component.html
         */
        {
            contentPath: __dirname + '/../contents/root-component-html.content',
            replace: [
                {
                    regex: /:nameCamel/g,
                    value: 'Root'
                }
            ],
            writePath: '/src/app/components/root/root.component.html'
        },
        /**
         * root.component.scss
         */
        {
            contentPath: __dirname + '/../contents/component-style.content',
            replace: [
                {
                    regex: /:nameKebap/g,
                    value: 'root'
                }
            ],
            writePath: '/src/app/components/root/root.component.scss'
        },

    ];
}

module.exports = async (options) => {
    let readFile;
    let writeFile;
    try {
        readFile = util.promisify(fs.readFile);
        writeFile = util.promisify(fs.writeFile);

        const toCreate = getToCreate(options);
        for (let i in toCreate) {
            if (toCreate.hasOwnProperty(i)) {
                let content = '';

                if (toCreate[i].hasOwnProperty('contentPath')) {
                    content = await readFile(toCreate[i].contentPath, 'utf-8');
                }

                if (toCreate[i].hasOwnProperty('replace') && toCreate[i].replace.length) {
                    for (let j in toCreate[i].replace) {
                        if (toCreate[i].replace.hasOwnProperty(j)) {
                            content = content.replace(toCreate[i].replace[j].regex, toCreate[i].replace[j].value);
                        }
                    }
                }

                await writeFile(`${process.cwd()}/${options.names.kebap}/${toCreate[i].writePath}`, content);
            }
        }
    } catch (e) {
        throw e;
    }
};
