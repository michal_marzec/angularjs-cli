const fs = require('fs');
const util = require('util');

module.exports = async (options) => {
    const exists = util.promisify(fs.exists);
    const mkdir = util.promisify(fs.mkdir);
    const writeFile = util.promisify(fs.writeFile);
    const readFile = util.promisify(fs.readFile);
    const appendFile = util.promisify(fs.appendFile);

    const dirExists = await exists(options.directory);

    if (dirExists) throw new Error('Directory exists, rename your directive');

    await mkdir(options.directory);

    let contentJs = await readFile(`${options.contentPath}directive-js.content`, 'utf-8');
    let contentHtml = await readFile(`${options.contentPath}directive-html.content`, 'utf-8');

    contentJs = contentJs.replace(/:nameCamel/g, options.names.camel);
    contentJs = contentJs.replace(/:nameKebap/g, options.names.kebap);

    contentHtml = contentHtml.replace(/:nameCamel/g, options.names.camel);

    if (options.root) {
        await appendFile(`${options.directory}/../../../../app.imports.js`, `\nrequire('./src/app/directives/${options.names.kebap}/${options.names.kebap}.directive.js');`);
    }

    await writeFile(`${options.directory}/${options.names.kebap}.directive.js`, contentJs);
    await writeFile(`${options.directory}/${options.names.kebap}.directive.html`, contentHtml);
};
