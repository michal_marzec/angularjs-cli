const fs = require('fs');
const util = require('util');

module.exports = async (options) => {
    const exists = util.promisify(fs.exists);
    const mkdir = util.promisify(fs.mkdir);
    const writeFile = util.promisify(fs.writeFile);
    const readFile = util.promisify(fs.readFile);
    const appendFile = util.promisify(fs.appendFile);

    const dirExists = await exists(options.directory);
    if (dirExists) throw new Error('Directory exists, rename your component');

    await mkdir(options.directory);

    let contentJs = await readFile(`${options.contentPath}component-js.content`, 'utf-8');
    let contentHtml = await readFile(`${options.contentPath}component-html.content`, 'utf-8');
    let contentStyle = await readFile(`${options.contentPath}component-style.content`, 'utf-8');

    contentJs = contentJs.replace(/:nameCamel/g, options.names.camel);
    contentJs = contentJs.replace(/:nameKebap/g, options.names.kebap);

    contentHtml = contentHtml.replace(/:nameCamel/g, options.names.camel);

    contentStyle = contentStyle.replace(/:nameKebap/g, options.names.kebap);


    if (options.root) {
        await appendFile(`${options.directory}/../../../../app.imports.js`, `\nrequire('./src/app/components/${options.names.kebap}/${options.names.kebap}.component.js');`);

        await appendFile(`${options.directory}/../../../../src/app/styles/components.scss`, `\n@import "../components/${options.names.kebap}/${options.names.kebap}.component";`);
    }

    await writeFile(`${options.directory}/${options.names.kebap}.component.js`, contentJs);
    await writeFile(`${options.directory}/${options.names.kebap}.component.scss`, contentStyle);
    await writeFile(`${options.directory}/${options.names.kebap}.component.html`, contentHtml);
};
