const fs = require('fs');
const util = require('util');

module.exports = async (options) => {
    try {
        const mkdir = util.promisify(fs.mkdir);

        await mkdir(`${process.cwd()}/${options.names.kebap}`);
        await mkdir(`${process.cwd()}/${options.names.kebap}/src`);
        await mkdir(`${process.cwd()}/${options.names.kebap}/src/assets`);
        await mkdir(`${process.cwd()}/${options.names.kebap}/src/app`);
        await mkdir(`${process.cwd()}/${options.names.kebap}/src/app/components`);
        await mkdir(`${process.cwd()}/${options.names.kebap}/src/app/components/root`);
        await mkdir(`${process.cwd()}/${options.names.kebap}/src/app/styles`);
        await mkdir(`${process.cwd()}/${options.names.kebap}/src/app/config`);
    } catch (e) {
        throw e;
    }
};
