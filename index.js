#!/usr/bin/env node

const READ = require('./interfaces/read.interface');
const readCommand = require('./helpers/read.helper');
const newCommand = require('./commands/new.command');
const generateCommand = require('./commands/generate.command');
const infoCommand = require('./commands/info.command');
const clear = require('clear');

// spinner
const ora = require('ora');

async function run (spinner) {
    const args = process.argv;
    args.splice(0, 2);

    const options = readCommand(args, spinner);

    switch (options.command) {
        case READ.COMMANDS.NEW:
            await newCommand(options, spinner);
            break;
        case READ.COMMANDS.GENERATE:
            await generateCommand(options, spinner);
            break;
        default:
            await infoCommand(options, spinner);
    }
}

(async function () {
    const spinner = ora('Executing... ').start();
    try {
        clear();
        await run(spinner);
    } catch (e) {
        spinner.stop();
        spinner.fail('Failure ' + e);
        return;
    }
    spinner.stop();
    spinner.succeed('Success! ');
})();
