const fs = require('fs');
const util = require('util');
const READ = require('./../interfaces/read.interface');
const createComponent = require('./../helpers/generate-create-component.helper');
const createDirective = require('./../helpers/generate-create-directive.helper');
const createFilter = require('./../helpers/generate-create-filter.helper');
const createConstant = require('./../helpers/generate-create-constant.helper');

module.exports = async (options, spinner) => {
    spinner.text = 'Executing \'generate\' command... ';
    const exists = util.promisify(fs.exists);
    options.root = await exists(`${process.cwd()}/.angularjs-cli.json`);
    const mkdir = util.promisify(fs.mkdir);
    if (options.root) {
        const srcDirectory = await exists(`${process.cwd()}/src`);
        if (!srcDirectory) await mkdir(`${process.cwd()}/src`);

        const appDirectory = await exists(`${process.cwd()}/src/app`);
        if (!appDirectory) await mkdir(`${process.cwd()}/src/app`);

        const typeDirectory = await exists(`${process.cwd()}/src/app/${options.type}s`);
        if (!typeDirectory) await mkdir(`${process.cwd()}/src/app/${options.type}s`);
    }

    options.directory = `${process.cwd()}`;

    if (options.root) {
        options.directory = `${options.directory}/src/app/${options.type}s/${options.names.kebap}`;
    } else {
        options.directory = `${options.directory}/${options.names.kebap}`;
    }

    const dirExists = await exists(options.directory);
    if (dirExists) throw new Error('Directory already exists, rename your component');

    options.contentPath = __dirname + '/../contents/';

    return new Promise(async (resolve, reject) => {
        try {
            switch (options.type) {
                case READ.TYPES.COMPONENT:
                    await createComponent(options);
                    break;
                case READ.TYPES.DIRECTIVE:
                    await createDirective(options);
                    break;
                case READ.TYPES.FILTER:
                    await createFilter(options);
                    break;
                case READ.TYPES.CONSTANT:
                    await createConstant(options);
                    break;
            }
            resolve();
        } catch (e) {
            reject(e);
        }
    });
};
