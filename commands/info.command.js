const figlet = require('figlet');
const chalk = require('chalk');

module.exports = function (options, spinner) {
    spinner.text = 'Executing \'info\' command... ';

    return new Promise(async (resolve, reject) => {
        console.log(chalk.red(figlet.textSync('AngularJS CLI')));

        resolve();
    });
};
