const fs = require('fs');
const util = require('util');
const createDirectories = require('./../helpers/new-create-directories.helper.js');
const createFiles = require('./../helpers/new-create-files.helper.js');
const triggerInstall = require('./../helpers/new-install-dependencies.helper.js');
const createComponent = require('./../helpers/generate-create-component.helper.js');
const READ = require('./../interfaces/read.interface.js');

module.exports = function (options, spinner) {
    spinner.text = 'Executing \'new\' command... ';
    const exists = util.promisify(fs.exists);

    return new Promise(async (resolve, reject) => {
        const alreadyCreated = await exists(`${process.cwd()}/${options.names.kebap}`);
        if (alreadyCreated) {
            reject(`Directory '${options.names.kebap}' already exists`);
            return;
        }
        const homeComponentOptions = {
            command: READ.COMMANDS.GENERATE,
            type: READ.TYPES.COMPONENT,
            names: {
                camel: 'PageHome',
                snake: 'page_home',
                kebap: 'page-home'
            },
            directory: `${process.cwd()}/${options.names.kebap}/src/app/components/page-home`,
            root: true,
            contentPath: __dirname + '/../contents/'
        };
        try {
            spinner.text = 'Creating directories... ';
            await createDirectories(options);
            spinner.text = 'Creating files... ';
            await createFiles(options);
            spinner.text = 'Creating components... ';
            await createComponent(homeComponentOptions);
            spinner.text = 'Installing dependencies... ';
            await triggerInstall(options);
        } catch(e) {
            reject(e);
            return;
        }

        resolve();
    });
};
