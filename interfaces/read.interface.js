module.exports = {
    MAIN: {
        NEW: 'new',
        GENERATE: 'generate',
        GENERATE_SHORT: 'g',
        INFO: undefined
    },
    COMMANDS: {
        NEW: 'new',
        GENERATE: 'generate',
        INFO: 'info'
    },
    TYPES: {
        COMPONENT: 'component',
        DIRECTIVE: 'directive',
        FILTER: 'filter',
        CONSTANT: 'constant'
    }
};
